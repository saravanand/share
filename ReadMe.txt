ReadMe
******

ReadMe document for the Patch for CelloSaaS Web Application migration from version 4.5.0.0 to version 4.5.0.1.

The steps are outlined as given below,

Steps
*****
1. Update the "TenantRegistrationController" inside the web application's "api" folder which contains the REST End points
2. Replace the existing "CelloSaaSDlls" folder with the ones in this patch
3. Update the "Controllers" folder with the contents in the patch
4. Update the services inside the "OAuthHelpers" with the contents in the patch
5. Update the views inside the "Views" with the contents in the patch
6. Perform the changes in the configurations as outlined in the below given sections Section (A) through (C)
7. After performing the following set of changes, stop the IIS Express and re run the application. Also restart the IIS and verify the application.

A) Web Config Changes
*********************
1) HttpModule registration
**************************
Kindly note that the below given modules are to be replaced in the "WebApplication"'s web.config file in the <httpmodules> section at two places

<httpModules> & <modules runAllManagedModulesForAllRequests="true"> sections

<remove name="FormsAuthenticationModule"/>
<add name="SessionAuthenticationModule" type="Microsoft.IdentityModel.Web.SessionAuthenticationModule, Microsoft.IdentityModel, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" />
<add name="WSFederationAuthenticationModule" type="Microsoft.IdentityModel.Web.WSFederationAuthenticationModule, Microsoft.IdentityModel, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" />
<add name="TenantSelfRegisterModule" type="CelloSaaS.View.TenantSelfRegisterModule, CelloSaaS.View" />
<add name="CelloClaimsRequestPreProcessor" type="CelloSaaS.View.HttpModules.CelloClaimsRequestPreProcessor,  CelloSaaS.View"/>
<add name="CelloRequestPreProcessor" type="CelloSaaS.View.CelloRequestPreProcessor, CelloSaaS.View" />
<add name="TenantLicenseValidatorHttpModule" type="CelloSaaS.View.TenantLicenseValidatorHttpModule, CelloSaaS.View" />
<add name="UrlSecurityHttpModule" type="CelloSaaS.View.UrlSecurityHttpModule, CelloSaaS.View" />

Include the binding redirections under assemblyBinding
<runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
	<dependentAssembly>
		<assemblyIdentity name="System.Net.Http.Formatting" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
		<bindingRedirect oldVersion="0.0.0.0-5.2.0.0" newVersion="5.2.2.0" />
	</dependentAssembly>
	<dependentAssembly>
		<assemblyIdentity name="System.Web.Http" publicKeyToken="31bf3856ad364e35" />
		<bindingRedirect oldVersion="0.0.0.0-5.2.0.0" newVersion="5.2.2.0" />
	</dependentAssembly>

B) AppSettings Changes
**********************
CelloSaaSApplicationMVC3\Config\AppSettings.config
Add the following keys in AppSettings.config
	<add key="DataConnectionString" value="DefaultEndpointsProtocol=https;AccountName=cello;AccountKey=abcd" />
	<add key="RedirectUriFallbackEnabled" value= "true" />
	<add key="AuthorizationCallbackUriSegment" value= "Account/AuthorizationCallback"/>
	<add key="ClientApplicationUri" value="http://localhost:58667/" />

C) Unity.config Changes
***********************
[CelloSaaSApplicationMVC3\Unity.config]
Add the following type alias under <typeAliases> section
	<typeAlias alias="ICelloExceptionService" type="CelloSaaS.Library.Exceptions.ICelloExceptionService,CelloSaaS.Library"/>
	<typeAlias alias="EnterpriseLibraryExceptionService" type="CelloSaaS.Library.Exceptions.EnterpriseLibraryExceptionService,CelloSaaS.Library"/>

Remove the below type from the DataAccess Container and add the same on Service Container.
<container name="DataAccess">
   <types>
	<type type="IFileStorageService" mapTo="FileStorageService" name="IFileStorageService" />

Add the following type under the Services Container.
<container name="Services">
   <types>
	<type type="ICelloExceptionService" mapTo="EnterpriseLibraryExceptionService" name="ICelloExceptionService"></type>